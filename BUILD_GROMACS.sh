#!/bin/sh

#copy this script into the main gromacs source code directory and run
#make single-threaded variant
rm -fr build
mkdir build
cd build
cmake3 -DCMAKE_INSTALL_PREFIX=${HOME}/gromacs_develop ..
make -j8
make install

#make mpi variant
cd ..
rm -fr build
mkdir build
cd build
cmake3 -DCMAKE_INSTALL_PREFIX=${HOME}/gromacs_develop -DGMX_MPI=on ..
make -j8
make install
