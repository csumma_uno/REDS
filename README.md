# REDS - Scripts and Tutorial

The **(REDS)** tutorial, meant to guide a user in setting up a simulation of replica exchange with dynamical scaling **(REDS)** using a new implementation of the **GROMACS** simulation package can be found in the [automate_REDS1 subdirectory's README.md file](https://gitlab.com/csumma_uno/REDS/-/blob/master/automate_REDS1/README.md?ref_type=heads).

For a more in-depth explanation of the theory and implementation of the REDS method, refer to the publication by [Rick, Schwing and Summa](https://pubs.acs.org/doi/10.1021/acs.jcim.0c01236?ref=pdf).

