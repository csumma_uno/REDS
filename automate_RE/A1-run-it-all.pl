#!/usr/bin/perl

if (scalar @ARGV != 4) {
	die ("Usage: A1-run-it-all.pl pdbname lowTemp highTemp seed\n");
}

my $pdbname = shift @ARGV;
my $lowTemp = shift @ARGV;
my $highTemp = shift @ARGV;
my $seed    = shift @ARGV;

my @result1 = `./A2-pdb-up-to-equil.pl $pdbname`;

print @result1;

my @result2 = `./A3RE-equil-through-production.pl $lowTemp $highTemp 0 $seed`;

print @result2;
