#!/bin/sh
rm *.gro
rm temp.* *.top 
rm \#*
rm *.itp
rm *.tpr
rm *.trr
rm *.log
rm *.edr
rm -fr equil*
rm -fr prod*
rm output.txt
rm mdout.mdp
