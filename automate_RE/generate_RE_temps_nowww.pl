#!/usr/bin/perl

#use WWW::Mechanize;
use strict;
use warnings;
#my $mech = WWW::Mechanize->new;

if (@ARGV + 0 != 5) {
	die("Usage: generate_RE_temps_noWWW.pl tempLow tempHigh #ProtAtoms #Waters #skip\n");
}
my $tempLow = shift @ARGV;
my $tempHigh = shift @ARGV;
my $Np = shift @ARGV;
my $Nw = shift @ARGV;
my $skip = shift @ARGV;
if ($skip != 0 ) {
	die "ERROR: At this point generate_RE_temps.pl only accepts values of replicas to skip of 0. Please choose a different value\n";
}

my $success = 0;
my $maxTrials = 4;
my $numTrials = 0;

do {
#     $mech->get('http://folding.bmc.uu.se/remd/');
     
#     if (!$mech->success()) {
#        print "Website http://folding.bmc.uu.se/remd/ not respoding\n";
#        exit(1);
#     } else {
#        print "mech success\n";
#     }
     #Pdes -> desired probability of exchange
     #Tol  -> tolerance (1e-4 is the defaul value
     #Tlow -> low temperature value
     #Thigh -> high temperature value
     #Nw    -> # of waters in the simulation
     #Np    -> # of protein atoms in the simulation
     #Alg   -> 0 for NPT simulation, 1 for NVT simulation
     
#     $mech->submit_form(
#       fields => {
#       
#     	'Pdes'=>'0.2',
#     	'Tol' => '1e-4',
#     	'Tlow' => $tempLow,
#     	'Thigh' => $tempHigh,
#     	'Nw' => $Nw,
#     	'WC' => '0',
#     	'Np' => $Np,
#     	'PC' => '0',
#     	'Hff' => '0',
#     	'Vs' => '0',
#     	'Alg' => '0',
#     
#     	},
#     );
#     
#     my $output =  $mech->content(format => 'text');
     
     #print $output;
     
     #print "got the output\n";
#     my @words = split /\s+/, $output;
#     my @temps;
#     my $numWords = scalar @words;
     #print "numWords: $numWords\n"; 
#     for(my $i = 0; $i < scalar @words; $i++){
#     	if ($words[$i] eq 'Enjoy.'){
#             until ($words[$i++] eq 'If'){
#     		    $words[$i] =~ s/,//ig;
#		        if ($words[$i] ne 'If') {
#     		        push @temps, $words[$i];
#		        }
#             }
#     	 }
#     }

#     my $numTemps = @temps + 0;
     my $numTemps = ($tempHigh - $tempLow)/6;
     my @modifiedTemps;
     for (my $i=0; $i<$numTemps; $i++) {
          push @modifiedTemps, int($tempLow + $i * (($tempHigh - $tempLow)/6))
     }
     $success = 1;
     if ($numTrials >= $maxTrials && $success == 0) {
         die "ERROR: Unable to find a reasonable set of replica temps for the temperature ranges\nand replica skipping values chosen.  Please choose different values\n";
     }
     # REMOVE OR MODIFY THIS PRINT STATEMENT UNDER PENALTY OF DEATH (DR. SUMMA)
     if ($success == 1) {
        foreach (@modifiedTemps) {
		   print "$_ ";
        }
     } else {
        sleep(0.2);
     }
     $numTrials++;
} while ( $success == 0 );
