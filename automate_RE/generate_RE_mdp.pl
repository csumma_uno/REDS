#!/usr/bin/perl

###############################################################
################	USAGE	###############################
#	perl generate_mdp.pl mdpTemplatePrefx temperature index
#	arg1 :a filename prefix fitting *_ZZZ.mdp (equil or prod)
#	arg2 :the temperatures to replace ZZZ with
#	arg3 :the index of this replica
#
###############################################################	

#use strict;
#use warnings;
#use diagnostics;

use File::Copy "cp";

#print "The arguments before 1 shift: @ARGV\n";

my $prefix = shift(@ARGV);
my $temperature = shift (@ARGV);
my $index = shift (@ARGV);

# generate new name
my $new_file = $prefix . "_" . $index . ".mdp";	
my $file_template = $prefix . "_" . "ZZZ.mdp";

#print $new_file;	
cp($file_template, $new_file) or die ("copy $new_file failed");
	
# Replace instances of ZZZ with the temp
`perl -pi -e 's/ZZZ/$temperature/g' $new_file`;

