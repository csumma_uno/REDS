#!/usr/bin/perl

if (scalar @ARGV != 4) {
	die ("Usage: A3-equil-through-production.pl lowTemp highTemp replica_skip seed\n");
}

my $lowTemp = shift @ARGV;
my $highTemp = shift @ARGV;
my $skip     = shift @ARGV;
my $seed    = shift @ARGV;

# THIS IS THE POINT WHERE WE SHOULD SPLIT THIS INTO TWO SCRIPTS.
# EVERYTHING PRIOR TO THIS POINT IS GETTING THE FILES SET UP FOR
# THE USER'S SPECIFIC SYSTEM. 
    
my @result8 = `cp em.gro restraint.gro`;

print @result8;

my @result9 = `./automate_RE.pl $lowTemp $highTemp $skip`;

if (@result9[scalar @result9 -1] =~ /ERROR/) {
	die "$result9[scalar @result9 -1]\n";
}

print @result9;
    
my $rawTemperatureString = @result9[@result9-1];
print "I think the temperature string is: $rawTemperatureString\n";
my @temperatureOutput = split /\s+/ , $rawTemperatureString;
my $numTemps = scalar @temperatureOutput;
print "numtemps = $numTemps\n";
my @temperatures;
for (my $i=0; $i < scalar @temperatureOutput; $i++) {
	push @temperatures, @temperatureOutput[$i]
}

my @subdirs_unsorted = `find . -type d | grep equil`;
my @subdirs = sort @subdirs_unsorted;

foreach $dir (@subdirs) {
	chomp $dir;
	#print $dir;
	my @result10 = `cd $dir; gmx grompp -f equil -c restraint.gro -p topol.top; cd ..`;
	print @result10;
}

#this is going to run the equilibration step
print "RE: BEGIN EQUILIBRATION RUN\n";
my $mpiexecPrefix = "mpiexec -np $numTemps gmx_mpi mdrun -v -multidir ";
my $mpiexecSuffix = " -s topol.tpr -reseed $seed ";
my $subdirlist = "";
foreach $dir (@subdirs) {
	chomp $dir;
	$subdirlist = $subdirlist . $dir . " ";
}
print "$subdirlist\n";
my $fullCommand = $mpiexecPrefix . $subdirlist . $mpiexecSuffix;

print "$fullCommand\n";

# now that the full equilibration command is built with appropriate subdirectory names, run it
my @result11 = `$fullCommand`;
print @result11;
print "RE: END EQUILIBRATION RUN\n";
#done equilibrating


print "SETTING UP FOR PRODUCTION RUN\n";
# ACTUAL PRODUCTION RUN FOLLOWS
# get set up for production run by copying equilibration directories
my @prodsubdirs;
foreach $dir (@subdirs) {
	chomp $dir;
	my $dirprod = $dir;
	$dirprod =~ s/equil/prod/;
	`cp -a $dir $dirprod`;
	push @prodsubdirs, $dirprod;
}

# at this point, @prodsubdirs size should be the same as @temperatures size
if (scalar @prodsubdirs != scalar @temperatures) {
	die "Something has gone horribly awry!!!";
}

for (my $i = 0; $i < scalar @prodsubdirs; $i++) {
    my $dir = @prodsubdirs[$i];
    my $temp = @temperatures[$i];
    chomp $dir;
    my @result14 = `./generate_RE_mdp.pl sim $temp $i; cd $dir; rm *.mdp; mv ../sim_$i.mdp ./sim.mdp; gmx grompp -f sim -c confout.gro -t state.cpt -p topol.top; cd ..`;
}


print "RE: BEGIN PRODUCTION RUN\n";
#this is the run step
#my @result15 = `mpiexec -np 6 gmx_mpi mdrun -v -multidir prod[012345] -s topol.tpr -replex 100 -reseed 175320 -REDS FALSE`;
$mpiexecPrefix = "mpiexec -np $numTemps gmx_mpi mdrun -v -multidir ";
$mpiexecSuffix = " -s topol.tpr -replex 100 -reseed $seed";
my $prodsubdirlist = "";
foreach $dir (@prodsubdirs) {
	chomp $dir;
	$prodsubdirlist = $prodsubdirlist . $dir . " ";
}
print "$prodsubdirlist\n";
my $prodFullCommand = $mpiexecPrefix . $prodsubdirlist . $mpiexecSuffix;
print "$prodFullCommand";
my @result15 = `$prodFullCommand`;
print @result15;
print "RE: END PRODUCTION RUN\n";
