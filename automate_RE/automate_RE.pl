#!/usr/bin/perl

use IPC::System::Simple qw(system capture);

if ( @ARGV + 0 != 3) {
	die("Usage: automate.pl tempLow tempHigh skip");
}

my $tempLow = shift @ARGV;
my $tempHigh = shift @ARGV;
my $skip     = shift @ARGV;

######### Parse Topology.topol for Num_waters and num_proteins ########

my $results0 = capture($^X, "parse_Topology.pl", @ARGS);

my @number_of_molecules = split / /, $results0; 

print "Number of protein atoms: @number_of_molecules[0]\n";
print "Number of water   atoms: @number_of_molecules[1]\n";
########## Generate replica temperatures using online resource ################
########## number_of_molecules = (# of protein molecules, # of water molecules)	####

my $results1 = capture($^X, "generate_RE_temps.pl", $tempLow, $tempHigh, @number_of_molecules,$skip);
#my $results1 = capture($^X, "generate_RE_temps_nowww.pl", $tempLow, $tempHigh, @number_of_molecules,$skip);

if ($results1 =~ /ERROR/) {
	die "$results1";
}

print "Results 1: $results1\n";

my @temps = split / /, $results1;

print "These are the temps I get back: @temps\n";

###########	http://www.perlmonks.org/?node_id=45928
######### Get files in current directory ##################

opendir my $dir, "." or die "Cannot open directory: $!";

my @files = readdir $dir;

closedir $dir;

###########

######## Get the name of the file template ###########

my $file_template;

for (my $i = 0; $i < scalar @files; $i++) {
#	print "$files[$i] ";
	if (($files[$i] =~ /ZZZ.mdp/)){
		$file_template = $files[$i];	
	}
}


###### The ZZZ file template #########
print "The file template: $file_template\n";


#### Call generator with args consisting of the template and the values of 
#### Temperatures to use as replacement vals for Z
for (my $i=0; $i< scalar @temps; $i++) {
    `./generate_RE_mdp.pl nvt @temps[$i] $i`;
}

system('./organize.pl');

print "\n"; 
foreach ( @temps ) {
	print "$_ ";
}
