#!/usr/bin/perl

if (scalar @ARGV != 1) {
	die ("Usage: A2-pdb-up-to-equil.pl pdbname\n");
}

my $pdbname = shift @ARGV;

my @result1 = `(echo 15; echo 3; echo 3) | gmx pdb2gmx -f $pdbname -o sys_processed.gro -water spce -ter`;

print @result1;

my @result2 = `gmx editconf -f sys_processed.gro -o sys_newbox.gro -c -d 1.0 -bt cubic`;

print @result2;

my @result3 = `gmx solvate -cp sys_newbox.gro -cs spc216.gro -o sys_solv.gro -p topol.top`;

print @result3;

my @result4 = `gmx grompp -f ions.mdp -c sys_solv.gro -p topol.top -o ions.tpr --maxwarn 1`;

print @result4;

#This is where ions would be added, if you need to add them.
#TODO write some code to parse output above, and add the ions

my @result5 = `cp sys_solv.gro sys_solv_ions.gro`;

print @result5;

my @result6 = `gmx grompp -f minim.mdp -c sys_solv_ions.gro -p topol.top -o em.tpr`;

print @result6;

my @result7 = `gmx mdrun -v -deffnm em`;

print @result7;
