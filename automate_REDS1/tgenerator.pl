#!/usr/bin/perl

# Author: Christopher M. Summa (perl port)
# Original Code (php) Author and Reference : Alexandra Patriksson and David van der Spoel, A temperature predictor for parallel tempering simulations Phys. Chem. Chem. Phys.,  10 pp. 2073-2077 (2008) http://dx.doi.org/10.1039/b716554d

########################################################
# USAGE
#
my $USAGE =<<USAGE;

     Usage:

         tgenerator.pl --low 275.5 --high 330.0 --prob 0.5 --nwat 1000 --nprot 800 [-help]


         required inputs:

             low:   temp (K) of lowest temperature replica 
             high:  temp (K) of highest  temperature 
             prob:  target probability of swap acceptance
             nwat:  number of water molecules
             nprot: number of protein molecules
             help:  Prints out this helpful message

        optional inputs and their default values
             tol:   tolerance (default = 0.0001) 
             protc: protein constraint (default = 0)
                       0 = Fully Flexible
                       1 = Bonds to hydrogens only
                       2 = All bonds
             watc: water constraints (default = 0)
                       0 = Fully Flexible
                       2 = Flexible angle
                       3 = Rigid
             hprot: hydrogens in protein (default = 0)
                       0 = All H
                       1 = Polar H
             virtual: Virtual sites in protein (default = 0)
                       0 = None
                       1 = Virtual Hydrogen

USAGE
#
######################################################

use strict;
use vars qw/ $opt_h $opt_l $opt_k $opt_p $opt_w $opt_n $opt_t $opt_c $opt_a $opt_f $opt_v/;
use Getopt::Long;
use Scalar::Util qw(looks_like_number);

GetOptions ('h|help' => \$opt_h,
            'l|low=f' => \$opt_l,
            'k|high=f' => \$opt_k,
            'p|prob=f' => \$opt_p,
            'w|nwat=i'  => \$opt_w,
            'n|nprot=i' => \$opt_n,
            't|tol=f'   => \$opt_t,
            'c|protc=i' => \$opt_c,
            'a|watc=i'  => \$opt_a,
            'f|hprot=i' => \$opt_f,
            'v|virtual=i' => \$opt_v);

if ($opt_h) {
    do_usage(0);
}

sub do_usage {
    print "$USAGE\n";
    exit $_[0] ;
}

sub myround {
    my ($x, $pow10) = @_[0];
    my $a = 10 ** $pow10;

    return (int($x / $a + (($x < 0) ? -0.5 : 0.5)) * $a);
}


# Input fields
my $inTlow  = $opt_l || do_usage(1);  # temperature of lowest temp replica
my $inThigh = $opt_k || do_usage(2);  # temperature of highest temp replica
my $inPdes =  $opt_p || do_usage(3);  # probability of exchange
my $inNw   =  $opt_w || do_usage(4);  # number of waters (integer)
my $inNp =    $opt_n || do_usage(5);  # number of protein atoms
my $inTol =   $opt_t || 0.0001;       # Tolerance (default 1x10-4)

# Multiple choice fields
my $inWC =    $opt_a || 0;            # water constraints
if ($opt_a == 0 || $opt_a == 2 || $opt_a == 3) {
   $inWC = $opt_a;
} else {
   do_usage(7);
}

my $inPC =    0;                      # protein constraints (0,1,2)
if ($opt_c == 0 || $opt_c == 1 || $opt_c == 2) {
    $inPC = $opt_c;
} else {
    do_usage(6);
}
my $inHff =   0;                      # Hydrogens in Protein (backbone=0, all polar=1)
if ($opt_f == 0 || $opt_f == 1 || $opt_f == 2) {
   $inHff = $opt_f;
} else {
   do_usage(8);
}
my $inVs =    $opt_v || 0;            # virtual sites
my $inAlg = 0;  # this can remain fixed for now

# Constants. May depend on input in principle (but not yet).
our $A0 = -59.2194;
our $A1 = 0.07594;
our $B0 = -22.8396;
our $B1 = 0.01347;
our $D0 = 1.1677;
our $D1 = 0.002976;
my $maxiter = 100;

my $kB       = 0.008314;
my $Tlow     = $inTlow;
my $Thigh    = $inThigh;
my $Nw       = $inNw;
my $Np       = $inNp;
my $Npp      = 0;
my $Hff      = $inHff;
my $Vs       = $inVs;
my $Pdes     = $inPdes;
my $PC       = $inPC;
my $NC       = 0;
my $VC       = 0;
my $Tol      = $inTol;
my $debug    = 0;
my $Alg      = $inAlg;
my $WC       = $inWC;
my $Hff      = $inHff;
my $Vs       = $inVs;

# Check input variables
my $error = 0;
if ( !looks_like_number($Pdes) || !looks_like_number($Tlow) || !looks_like_number($Thigh) ||
     !looks_like_number($Np) || !looks_like_number($Nw) || !looks_like_number($Tol) ) {
  print "ERROR : Some of your inputs are not numbers\n";
  $error++;
}
if ( ($Pdes > 1) || ($Pdes < 0) ) {
  print "ERROR: You have to give a probability Pdes: between 0 and 1!\n";
  $error++;
}
if ( $Thigh <= $Tlow ) {
  print "ERROR: The lower limit of the temperature range has to be below the upper limit - Check your input!\n";
  $error++;
}
if ( ($Tlow <= 0) || ($Thigh <= 0) ) {
  print "ERROR: You must have temperatures that are > 0!\n";
  $error++;
}
if ( $Np==0 ) {
  print "ERROR: You can not have zero atoms in protein!\n";
  $error++;
}
if ( $Alg != 0) {
  print "ERROR: Can not do constant volume yet!\n";
  $error++;
}

sub calc_mu {

    my $Nw = $_[0];
    my $Np = $_[1];
    my $Temp = $_[2];
    my $FEner = $_[3];
    
    return (($A0+$A1*$Temp)*$Nw +
            ($B0+$B1*$Temp)*$Np -
            $Temp*$FEner);

} # end subroutine calc_mu


sub erfc {
    my $x = $_[0];
    # constants
    my $a1 =  0.254829592;
    my $a2 = -0.284496736;
    my $a3 =  1.421413741;
    my $a4 = -1.453152027;
    my $a5 =  1.061405429;
    my $p  =  0.3275911;

    # Save the sign of x
    my $sign = 1;
    if ($x < 0) {
        $sign = -1;
    }
    $x = abs($x);

    # Abramowitz, M. and Stegun, I. A. 1972. 
    # Handbook of Mathematical Functions with Formulas, Graphs, and Mathematical Tables. Dover.
    # Formula 7.1.26
    my $t = 1.0/(1.0 + $p*$x);
    my $y = ((((($a5*$t + $a4)*$t) + $a3)*$t + $a2)*$t + $a1)*$t*exp(-$x*$x);

    return $sign*$y;
}

sub myeval {
  my $m12 = $_[0];
  my $s12 = $_[1];
  my $CC  = $_[2];
  my $u   = $_[3];
  my $argument = -$CC*$u - ($u-$m12)*($u-$m12)/(2*$s12*$s12);
  return exp($argument);
}

sub myintegral {
  my $m12 = $_[0];
  my $s12 = $_[1];
  my $CC  = $_[2];
  my $int  = 0.0;
  my $umax = $m12+5*$s12;
  my $du   = $umax/100;
  if ($debug > 1) {
    print("umax = $umax m12 = $m12 s12 = $s12 CC = $CC");
  } 
  for(my $u = 0; $u<$umax; $u+=$du) {
    my $di = myeval($m12, $s12, $CC, $u+$du/2);
    $int += $di;
  } 
  my $pi = 3.14159265358979;
  return $du*$int/($s12*sqrt(2*$pi));
}   

my $Nh;
my $Nprot;

if ($error == 0) {
  $Npp = 0;
  $Nprot = 0;
  if ($Hff == 0) {
    $Nh = myround($Np*0.5134);
    if ($Vs == 1) {
      $VC = myround(1.91*$Nh);
    }
    $Nprot = $Np;
  }
  else {
    $Npp = myround($Np/0.65957);
    $Nh = myround($Np*0.22);
    if ($Vs == 1) {
      $VC = myround($Np+1.91*$Nh);
    }
    $Nprot = $Npp;
  }

  if ($PC == 1) {
    $NC = $Nh;
  }
  elsif ($PC == 2) {
    $NC = $Np;
  }
  
  my $Ndf      = (9-$WC)*$Nw + 3*$Np-$NC-$VC;
  my $FlexEner = 0.5*$kB*($NC+$VC+$WC*$Nw);

  print("Summary of input and derived variables.\n");
  print("Pdes " . $Pdes . "\n");
  print("Temperature range " . $Tlow . " - " . $Thigh . "\n");
  print("Number of water molecules " . $Nw . "\n");
  print("Number of protein atoms " . $Np . "\n");

  if ( $Npp > 0) {
      print ("Including all H " .  $Npp . "\n");
  }

  print ("Number of hydrogens in protein " . $Nh . "\n");
  print ("Number of constraints " .  $NC . "\n");
  print ("Number of vsites " . $VC . "\n");
  print ("Number of degrees of freedom " .  $Ndf . "\n");
  printf ("Energy loss due to constraints %.2f (kJ/mol K) \n",$FlexEner);

  my $index = 1;
  my @T;
  my @P;
  my @Siigma;
  my @Muu;
  my @MM;
  my @SS;
  $T[$index] = $Tlow;
  
  while ( ($T[$index] < $Thigh) ) {
    my $piter   = 0;
    my $forward = 1;
    my $iter    = 0;
    my $T1      = $T[$index];
    my $T2      = $T1+1;
    if ( $T2 >= $Thigh ) { 
       $T2 = $Thigh;
    }
    my $low     = $T1;
    my $high    = $Thigh;
    if ($debug == 2) {
      printf("Index %d, T1 = %f, T2 = %f\n", $index, $T1, $T2);
    }
    while ( (abs($Pdes-$piter) > $Tol) && ($iter < $maxiter) ) {
      $iter++;
      my $mu12 = ($T2-$T1) * (($A1*$Nw)+($B1*$Nprot)-$FlexEner);
      $MM[$index] = $mu12;
      
      my $CC    = (1/$kB) * ( (1/$T1)-(1/$T2) );
      my $Delta = $CC*$mu12;
      
      my $var = $Ndf*($D1*$D1*( $T1*$T1 + $T2*$T2 ) +
		        2*$D1*$D0*($T1+$T2) +
		        2*$D0*$D0);
      
      my $sig12 = sqrt($var);
      $SS[$index] = $sig12;
      
      if ($sig12 == 0) {
	      print "Sigma = 0\n";
	      exit(1);
      }
      #// I1
      my $erfarg1 = $mu12/($sig12*sqrt(2.0));
      my $I1      = 0.5*(erfc($erfarg1));

      #// I2
      #// Old analytical code according to the paper, however
      #// this suffers from numerical issues in extreme cases.
      #// $exparg  = $CC*(-$mu12 + $CC*$var/2);
      #// $erfarg2 = ($mu12 - $CC*$var)/($sig12*sqrt(2));
      #// $I2      = 0.5*exp($exparg)*(1.0 + erf($erfarg2));
      #// Use numerical integration instead.
      my $I2      = myintegral($mu12, $sig12, $CC);
      $piter   = ($I1 + $I2);
      
      if ($debug == 2) {
         printf("\n");
      	 printf("DT = %.3f CC = %.4f\n", $T2-$T1, $CC);
	     printf("mu12 = %.1f, sig12 = %.1f, Delta = %.1f\n",
               $mu12, $sig12, $Delta);
	     printf("erfarg1 = %.4f, Integral1 = %.4f\n", $erfarg1, $I1);
	     #//printf("exparg = %.2e, erfarg2 = %.1f<br>", $exparg, $erfarg2);
         #//printf("myintegral = %.4f, Integral2 = %.4f<br>", $myint, $I2);
	     printf("piter = %.3f\n", $piter);
         printf("\n");
      }
      
      if ( $piter > $Pdes ) {
	      if ( $forward==1 ) {
	         $T2 = $T2 + 1.0; 
	      }
	      elsif ( $forward==0 ) {
	         $low = $T2;
	         $T2 = $low + (($high-$low)/2);
	      }
          if ( $T2 >= $Thigh ) { 
             $T2 = $Thigh;
          }      
      }
      elsif ( $piter < $Pdes ) {
	      if ( $forward==1 ) {
	         $forward = 0;
	         $low = $T2 - 1.0;
	      }
	      $high = $T2;
	      $T2 = $low + (($high-$low)/2);
      }
    }
    
    #print("DEBUG piter = " . $piter . "\n");
    $P[$index]      = $piter;
    $Siigma[$index] = sqrt($Ndf)* ($D0 + $D1*$T1);
    $Muu[$index]    = calc_mu($Nw,$Nprot,$T1,$FlexEner);
    
    $index++;
    $T[$index] = $T2;
  }
  $Siigma[$index] = sqrt($Ndf)* ($D0 + $D1*$T[$index]);
  $Muu[$index] = calc_mu($Nw,$Nprot,$T[$index],$FlexEner);


  my $k;
  for($k=1; ($k<=$index); $k++) {
      printf("%d  %.2f  ",$k,$T[$k]);
      if ($k == 1) {
	    printf("%.0f  %.2f  ", $Muu[$k],$Siigma[$k]);
      }
      else {
	    printf("%.0f  %.2f  %.1f %.2f  %.4f ", $Muu[$k],$Siigma[$k],$MM[$k-1], $SS[$k-1],$P[$k-1]);
      }
      print("\n");
  }  

  print("\n");
  print("\n");

  printf("For your scripting pleasures we also give the temperatures below as one long comma-separated string. Enjoy.\n");
  for($k=1; ($k<$index); $k++) {
     printf("  %.2f,",$T[$k]);
  }
  printf("  %.2f\n",$T[$k]);

  print("If you use the results from this webserver in simulations which are published in scientific journals, please cite:\n");
  print(" Alexandra Patriksson and David van der Spoel, A temperature predictor for parallel tempering simulations Phys. Chem. Chem. Phys.,  10 pp. 2073-2077 (2008) http://dx.doi.org/10.1039/b716554d\n");
  print("\n");
  print("\n");
  print(" We also recommend the following literature about theory behind replica exchange simulations [1,2] and applications of REMD [3,4]. A recent review about sampling is in ref. [5].\n");

  print(" K. Hukushima and K. Nemoto: Exchange Monte Carlo Method and Application to Spin Glass Simulations J. Phys. Soc. Jpn. 65 pp. 1604-1608 (1996)\n");

  print(" T. Okabe and M. Kawata and Y. Okamoto and M. Mikami: Replica-exchange {M}onte {C}arlo method for the isobaric-isothermal ensemble Chem. Phys. Lett. 335 pp. 435-439 (2001)\n");

  print(" Marvin Seibert, Alexandra Patriksson, Berk Hess and David van der Spoel: Reproducible polypeptide folding and structure prediction using molecular dynamics simulations J. Mol. Biol. 354 pp. 173-183 (2005)\n");

  print(" David van der Spoel and M. Marvin Seibert: Protein Folding Kinetics and Thermodynamics from Atomistic Simulations Phys. Rev. Lett. 96 pp. 238102 (2006)\n");

  print(" H. X. Lei and Y. Duan: <i>Improved sampling methods for molecular simulation Curr. Opin. Struct. Biol. 17 pp. 187-191 (2007)\n");


}
