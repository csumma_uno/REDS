#!/usr/bin/perl

if (scalar @ARGV != 5 && scalar @ARGV != 6) {
	die ("Usage: A1-run-it-all.pl pdbname lowTemp highTemp replica_skip seed directory_to_create_and_pipe_output \n");
}

my $dir_name = "./temp";
if (scalar @ARGV == 6) {
    $dir_name = $ARGV[5];
}

my $pdbname = $ARGV[0];
my $lowTemp = $ARGV[1];
my $highTemp = $ARGV[2];
my $skip     = $ARGV[3];
my $seed    = $ARGV[4];


system("mkdir $dir_name");

system("touch timestamp1");

my @result1 = `./A2-pdb-up-to-equil.pl $pdbname`;

print @result1;

my @result2 = `./A3-equil-through-production.pl $lowTemp $highTemp $skip $seed`;
print @result2;

#uncomment the following if you want the script to move all the files
#into a separate subdirectory and clean up after itself

#system('sleep 1m');
#system("touch timestamp2");

#system("find . -newer timestamp1 ! -newer timestamp2 | xargs tar --no-recursion -czf $dir_name/$dir_name.tgz");
#system('./cleanup.sh');
#system('rm timestamp*');
