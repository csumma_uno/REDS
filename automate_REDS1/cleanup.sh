#!/bin/sh
rm *.gro
rm *.top 
rm \#*
rm *.itp
rm *.tpr
rm *.trr
rm *.log
rm *.edr
rm -fr equil*
rm -fr preprod*
rm -fr prod*
rm REDS*
rm -fr OPT*
rm mdout.mdp
