#!/usr/bin/perl -w
#
#

my $filename = $ARGV[0];
my $THEFILE;
my $nl;
my $sn;
my $sx;
my $sx2;
my $sx3;
my $sx4;
my $sy;
my $syx;
my $syx2;
my $ai;
my $bi;
my $ci;
my $di;
my $ei;
my $fi;
my $det;
my $bb;
my $cc;
my $dd;
$nl = 0;
$sn  = 0;
$sx  = 0;
$sx2  = 0;
$sx3  = 0;
$sx3  = 0;
$sx4  = 0;
$sy  = 0;
$syx  = 0;
$syx2  = 0;


open ($THEFILE,$filename) or die "Couldn't open file $filename\n";
while (<$THEFILE>)
{
    chomp;
    my @fields = split(/\s+/,$_);
    $sn = $sn + $fields[2];
    $sx = $sx + $fields[0] * $fields[2];
    $sx2 = $sx2 + $fields[0] * $fields[0] * $fields[2];
    $sx3 = $sx3 + $fields[0] * $fields[0] * $fields[0] * $fields[2];
    $sx4 = $sx4 + $fields[0] * $fields[0] * $fields[0] * $fields[0] * $fields[2];
    $sy  = $sy + $fields[1] * $fields[2];
    $syx = $syx + $fields[0] * $fields[1] * $fields[2];
    $syx2 = $syx2 + $fields[0] * $fields[0] * $fields[1] * $fields[2];
    $nl =  $nl + 1;
}
close $THEFILE;
if ( $nl == 2 ) { 
# do linear fit only
$det = $sn*$sx2 - $sx*$sx ;
$bb = ($sx2*$sy-$sx*$syx)/$det ;
$cc = ($sn*$syx-$sx*$sy)/$det*0.50 ;
$dd = 0.0 ; 
}   
if ( $nl >= 3) {
$det = $sn*($sx2*$sx4-$sx3*$sx3) - $sx*($sx*$sx4-$sx3*$sx2) + $sx2*($sx*$sx3-$sx2*$sx2);
$ai = ($sx2*$sx4-$sx3*$sx3)/$det ;
$bi = -($sx*$sx4-$sx3*$sx2)/$det ;
$ci = ($sx*$sx3-$sx2*$sx2)/$det ;
$di = ($sn*$sx4-$sx2*$sx2)/$det ;
$ei = ($sx2*$sx-$sn*$sx3)/$det ;
$fi = ($sn*$sx2-$sx*$sx)/$det ;
$bb = $sy*$ai+$syx*$bi+$syx2*$ci ;
$cc=($sy*$bi+$syx*$di+$syx2*$ei)*0.50 ;
$dd=($sy*$ci+$syx*$ei+$syx2*$fi)/3.0 ;
}
print "$bb $cc $dd\n";
