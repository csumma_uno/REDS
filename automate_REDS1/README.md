# Introduction


In this tutorial, we will guide a user into setting up a simulation of replica exchange with dynamical scaling **(REDS)** using a new implementation of the **GROMACS** simulation package. The **REDS** method was developed to counteract one of the major disadvantages of replica exchange molecular dynamics **(REMD)**, which is poor system size scaling. By utilizing dynamical scaling, a scaled replica is placed between two unscaled replicas. By substituting in scaled replicas to replace standard replicas, the system size of the simulation can be significantly reduced.  For a more in-depth explanation of the theory and implementation of the REDS method, refer to the publication by [Rick, Schwing and Summa](https://pubs.acs.org/doi/10.1021/acs.jcim.0c01236?ref=pdf).


# Setup/Methods

To begin, the user needs to first set up the [REDS implemented GROMACS simulation package](https://gitlab.com/csumma_uno/gromacs) as well as the scripts needed for [setting up the simulation](https://gitlab.com/csumma_uno/REDS), both found on GitLab. Additionally, the user also needs to also have access to Perl (currently Perl v5.36) in order to run the scripts necessary for the setup.  While the Perl scripts provided from GitLab will run all the necessary scripts when running the main one, **A1-run-it-all.pl**, a short explanation of the scripts provided is given.  

Also, obtain all the necessary Perl modules needed to run the simulation. CPAN can be used with the command cpan App::cpanminus to prepare to install the modules followed by using the command cpanm Module::Name (ex. cpanm Math::Round) to get the modules needed. Please note that there can be issues when processing the scripts through Windows due to the specific file formatting that causes the scripts to malfunction. 

* As mentioned already, the initial perl script **A1-run-it-all.pl** will run the entire script chain to provide all the necessary input files for the simulation.  There are six arguments that need to be provided when running the script; name of the pdb file, the lower limit of temperature for the simulation, the higher limit of temperature for the simulation, amount of replicas to skip between the unscaled replicas, seed number for the simulation in the cluster, and the name for the temporary directory to store all the files used in the script. It is recommended to set the replica skip value between 7-10 as higher values could result in issues with the analysis. If the temperature range and replica skip value leads to an even number of temperatures for the simulation, the setup will fail and an adjusted temperature range or skip value needs to be provided. 

* **A2-pdb-up-to-equil.pl** will set up the GROMACS configuration file using the pdb file provided as one of the arguments in **A1-run-it-all.pl**. The script will run the GROMACS commands *gmx pdb2gmx*, *editconf*, *solvate*, *grompp*, and *mdrun*.  If the GROMACS configuration file is already provided, instead of running **A1-run-it-all.pl** or **A2-pdb-up-to-equil.pl**, skip to running the **A3-equil-through-production.pl** instead. 

* **A3-equil-through-production.pl** will generate the directories, mdp files, and the configuration files needed to run the equilibration and production simulations. This script will call on and run **automate_REDS1.pl, parse_topology.pl, tgenerator.pl, generate_REDS1_mdp.pl and organize.pl** scripts.  The **organize.pl** script will be used to organize the various directories needed for the files being created. Depending on your system, the mpiexec command in **A3-equil-through-production.pl** on lines 61, 183 will need -n or -np to indicate the number of copies to run indicated by the number of temperatures. 

* The **automate_REDS1.pl** script will provide the topology file through the **parse_Topology.pl** script as well as the temperatures to be used for the replicas using the **generate_REDS1_temps.pl**. To obtain the temperatures, the script will call upon the **tgenerator.pl** script adapted for REDS usage from the temperature generator for REMD-simulations based on the work of [Patriksson and Spoel](https://virtualchemistry.org/remd-temperature-generator/).  The **generate_REDS1_mdp.pl** provides the .mdp files needed for the various replicas in the simulation, using the template .mdp file **sim_ZZZ.mdp**. Adjust the timesteps to what is needed on this file. 
    * The default value of the exchange probability used in **tgenerator.pl** is set to 0.5. To change the value, edit line 24 in the file **generate_REDS1_temps.pl** and change the --prob variable to a value between 0 and 1.

At the end of the setup, a directory containing the replicas starting from prod000 up to the number of replicas used for the simulation will be created. The unscaled replicas will be in the even numbered folders starting from prod000 and the scaled replicas in the odd numbered folders starting from prod001. Once completed, move the prodxxx folders to the desired working directory and the cleanup.sh script can be used to remove any setup files created. 



# Equilibration/Production

After the initial production run using the setup, the mdp files for the scaled replicas will need to be adjusted using the files from the automate_REDS1 folder. This includes the scripts **redsupdateabc.sh**, **redsupdatezeta.sh**; and the executable files **fit.pl**, and **histogram_and_average.pl**. In order to use the scripts, the REDS0xx.txt file will need to be changed/concatenated to the REDS.all file. Afterwards, run both the **redsupdateabc.sh** and **redsupdatezeta.sh** scripts in the scaled replica odd folders prod001, prod003, etc, for the new REDS variable values in the respective mdp files.  The REDS variables are not changed in the even folders containing the unscaled replica prod000, prod002, etc. 

A sample run **redsrun.sh** file is also provided. The **redsrun.sh** script will show a default run script that can be adjusted to match the number of replicas and show a setup to run all the replicas at once as well as automatically adjusting the REDS variables in the necessary folders. The variable **nn** refers to the current run version of the simulation and will need to be increased after each run.  For each run, it is advisable to provide a new reseed number.  The fit.in file will have the λ value between 0 and 1, and the number of times the simulation reached those λ values.  The results should be distributed fairly across the λ values, with an emphasis on the endpoints 0 and 1 that should be checked after the simulation.  

For the folders containing the unscaled replicas, the .mdp files will have constant values for the inputs **a_reds**, **b_reds**, **c_reds**, **zeta_init_reds** throughout the simulation. Meanwhile, the odd folders containing the scaled replicas will have their input values adjusted using the at the end of each run.  It is advisable to check that the new variable values as well as the zeta (the ending λ value of the previous run, and new λ value of the new run) are properly updated after each run.  


# Post-production Analysis

Possible post-production analysis using Gromacs include calculating the radius of gyration (gmx gyrate), radial distribution function (gmx rdf), secondary structure analysis (gmx do_dssp), various energy calculations (gmx energy), etc.


