import sys
import numpy as np
import math
from scipy.optimize import curve_fit
# uncomment the following line if you want to visualize your fit
#import matplotlib.pyplot as plt

zeta = []
EpotAtZeta = []
sd = []
artificialPoint = 0

for line in sys.stdin:
    zetain,Epot,sdin = line.split(" ")  #splits each line at the space between individual numbers and assigns them to the variables <>xin<> and <>yin<> respectively
    zetaval = float(zetain) #takes the value in the first column and assigns it to <>zetaval<>
    Epotval = float(Epot) #takes the value in the second column and assigns it to <>yvalues<>
    sdval = float(sdin)
    sdval = 1./math.sqrt(sdval)
    zeta.append(zetaval)    #inserts the x and y values in each row into the <>insert<> list   
    EpotAtZeta.append(Epotval)
    sd.append(sdval)

if len(zeta) == 2 :
    zeta.append((zeta[0] + zeta[1])/2)
    EpotAtZeta.append((EpotAtZeta[0] + EpotAtZeta[1])/2)
    artificalPoint = 1

# next line for debugging only
#    print (zetaval,Epotval)


# optimal value of a,b,c for REDS1 in GROMACS should follow the following relationship
#    <f(zeta)> = -((BetaLeft - BetaRight)/BetaCurrent)*<Epot(zeta)> -a -2*b*zeta -3*c*zeta^2 = 0
#    rearranging, and if we output -((BetaLeft - BetaRight)/BetaCurrent)*<Epot(zeta)> and call it "Y"
#    Y = a + 2*b*zeta  + 3*c*zeta^2
#    Thus, the bias funtion definition below
def biasfunc(zeta, a, b, c):
    return a + (2.0 * b * zeta) + (3.0 * c * zeta * zeta);
    #return -1.0 * a - (2.0 * b * zeta) - (3.0 * c * zeta * zeta);

zetaData = np.array(zeta)
EpotAtZetaData = np.array(EpotAtZeta)
sdData = np.array(sd)

popt, pcov = curve_fit(biasfunc, zetaData, EpotAtZetaData,p0=None, sigma=sdData)
print (popt)

if artificialPoint == 1 :
    print ("NOTE: fit with artificial third point\n")

# uncomment the following lines if you want to visualize your fit
#plt.plot(zetaData, biasfunc(zetaData, *popt), 'r-',
#        label='fit: a=%7.2f, b=%7.2f, c=%7.2f' % tuple(popt))
#plt.xlabel('zeta')
#plt.ylabel('Epot(Zeta)')
#plt.legend()
#plt.show()
