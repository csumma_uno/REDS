#!/usr/bin/perl -w

use strict;

if ( scalar @ARGV != 3) {
    die "usage: compareHistogramToEvenDistibution.pl filename column expectedBins";
}

my $filename = $ARGV[0];
my $columnIndex = $ARGV[1]+0;
my $expectedBins = $ARGV[2];
my $THEFILE;

my @vals;
my $sum = 0;
open ($THEFILE,$filename) or die "Couldn't open file $filename\n";
while (<$THEFILE>)
{
	chomp;
    my $templine = $_;
    my @fields = split(/\s+/,$_);
    push @vals,$fields[$columnIndex];
    $sum += $fields[$columnIndex];
}
close $THEFILE;
my $numVals = scalar @vals;
#print "$sum  $numVals\n";
my $even = $sum / $expectedBins;
my $sumdiff = 0;
foreach my $val (@vals) {
    $sumdiff = abs($val - $even);
}
my $percentDiff = ($sumdiff / $sum ) * 100;
print "$percentDiff\n";
