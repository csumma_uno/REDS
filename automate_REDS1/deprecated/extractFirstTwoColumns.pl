#!/usr/bin/perl

use strict;
my $filename = shift(@ARGV);

my $FILE;
open ($FILE,$filename) or die "Couldn't open file $filename\n";
while (<$FILE>)
{
    my @fields = split(/\s+/, $_);
    print "$fields[0] $fields[1]\n";
}
