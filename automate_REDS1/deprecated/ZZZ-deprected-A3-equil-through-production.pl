#!/usr/bin/perl

if (scalar @ARGV != 4) {
	die ("Usage: A3-equil-through-production.pl lowTemp highTemp replica_skip seed\n");
}

my $lowTemp = shift @ARGV;
my $highTemp = shift @ARGV;
my $skip     = shift @ARGV;
my $seed    = shift @ARGV;

my $PYTHON3 = "python3";
#my $PYTHON3 = "python3.6";

# THIS IS THE POINT WHERE WE SHOULD SPLIT THIS INTO TWO SCRIPTS.
# EVERYTHING PRIOR TO THIS POINT IS GETTING THE FILES SET UP FOR
# THE USER'S SPECIFIC SYSTEM. 
    
my @result8 = `cp em.gro restraint.gro`;

print @result8;

my @result9 = `./automate_REDS1.pl $lowTemp $highTemp $skip`;

if (@result9[scalar @result9 -1] =~ /ERROR/) {
	die "$result9[scalar @result9 -1]\n";
}

print @result9;
    
my $rawTemperatureString = @result9[@result9-1];
print "REDS1: I think the temperature string is: $rawTemperatureString\n";
my @temperatureOutput = split /\s+/ , $rawTemperatureString;
my $numTemps = scalar @temperatureOutput;
print "REDS1: numtemps = $numTemps\n";
my @temperatures;
for (my $i=0; $i < scalar @temperatureOutput; $i++) {
	push @temperatures, @temperatureOutput[$i]
}

if ($numTemps % 2 != 1) {
	die "Number of replicas (i.e. temps) should be odd.  Choose different temp range\n";
}

my @subdirs_unsorted = `find . -type d | grep equil`;

for (my $i = 0; $i<@subdirs+0; $i++) {
   $subdirs_unsorted[$i] =~ s/\.\///;
}

my @subdirs = sort @subdirs_unsorted;

foreach $dir (@subdirs) {
	chomp $dir;
	#print $dir;
	my @result10 = `cd $dir; gmx grompp -f equil -c restraint.gro -p topol.top; cd ..`;
	print @result10;
}
#this is going to run the equilibration step
print "REDS1: BEGIN EQUILIBRATION RUN\n";
my $mpiexecPrefix = "mpiexec -np $numTemps gmx_mpi mdrun -v -multidir ";
my $mpiexecSuffix = " -s topol.tpr -reseed $seed ";
my $subdirlist = "";
foreach $dir (@subdirs) {
	chomp $dir;
	$subdirlist = $subdirlist . $dir . " ";
}
print "REDS1: subdirlist is: $subdirlist\n";
my $fullCommand = $mpiexecPrefix . $subdirlist . $mpiexecSuffix;
print "REDS!: full command is: $fullCommand\n";
my @result11 = `$fullCommand`;
print @result11;
print "REDS1: END EQUILIBRATION RUN\n";
#done equilibrating
#
# begin setup for pre-production optimization loop
# setup arrays for a,b,c biasing constants - one value per replica
my @aVals;
my @bVals;
my @cVals;
for (my $i=0; $i < $numTemps; $i++) {
    push @aVals, 20.0;
    push @bVals, 20.0;
    push @cVals, 20.0;
}

#set up biasing parameters for the unflattened replicas - these will not be optimized
for (my $repID=0; $repID < $numTemps; $repID++) {
    if ( ($repID % 4) - 2 == 0 ) {
        print "Replica: $repID should have zeta near 0\n";
        # this replica should have a zeta value that tends toward 0
        $aVals[$repID] = 0;
        $bVals[$repID] = 250;
        $cVals[$repID] = 0;
    }
    elsif ( $repID % 4 == 0 ) {
        print "Replica: $repID should have zeta near 1\n";
        # this replica should have a zeta value that tends toward 1
        $aVals[$repID] = -500;
        $bVals[$repID] = 250;
        $cVals[$repID] = 0;
    }
}
# set up a data structure so we can track when the flattened replicas
# have sufficiently optimized biasing potentials
my @biasingOptimized;
for (my $repID = 0; $repID < $numTemps ; $repID++) {
    # odd replicas are flattened
    if ( $repID % 2 == 1) {
        # initially, they should all be "false"
        push @biasingOptimized, 0;
    } else {
        push @biasingOptimized, 1;
    }
}
my $fullyBiasOptimized = 0;

# code for setting up data structures to hold initial zeta values and zeta maxstep values
my @initalZetaVals;
my @zetaMaxStepVals;
for (my $repID = 0; $repID < $numTemps ; $repID++) {
    if ( $repID % 2 == 1 )     
    {     
         push @initalZetaVals, 0.5;
         push @zetaMaxStepVals, 0.1;
    }     
    elsif ( ($repID % 4) -2 == 0 )
    {     
         push @initalZetaVals, 0.1;
         push @zetaMaxStepVals, 0.1;
    }     
    elsif ($repID % 4 == 0) 
    {     
         push @initalZetaVals, 0.9;
         push @zetaMaxStepVals, 0.1;
    }     
      else  
    {     
         push @initalZetaVals, 0.0;
         push @zetaMaxStepVals, 0.1;
    }     
}

# begin pre-production optimization loop for biasing parameters of flattened replicas
print "REDS1: BEGIN OPTIMIZATION PHASE\n";
my $maxOptimizationTrials = 8;
my $optimizationTrials = 0;
while ( !($optimizationTrials > $maxOptimizationTrials) && !($fullyBiasOptimized == 1) )
{
     my $optTrialString = sprintf("%03i",$optimizationTrials);
     # get set up for pre-production run by copying equilibration directories
     my @preprodsubdirs;
     # for the first round, start the run from the end result of equilibration

     if ($optimizationTrials == 0) {
         foreach $dir (@subdirs) {
         	chomp $dir;
         	my $dirpreprod = $dir;
         	$dirpreprod =~ s/equil/preprod/;
            `mkdir $dirpreprod`;
         	`cp $dir\/confout.gro $dirpreprod`;
         	`cp $dir\/topol.top $dirpreprod`;
         	`cp $dir\/state.cpt $dirpreprod`;
         	push @preprodsubdirs, $dirpreprod;
         }
     } else {
     # in all other cases, copy input data from the previous optimization trial
         my $localcounter = 0;
         foreach $dir (@subdirs) {
         	chomp $dir;
         	my $dirpreprod = $dir;
         	$dirpreprod =~ s/equil/preprod/;
            my $priorOptTrialString = sprintf("%03i",$optimizationTrials-1);
            my $priorOptDir = "OPT_ROUND_" . $priorOptTrialString;
            # the line we are looking for to grab the previous zeta value looks like:
            # !REDS MYREPL 0 : ZETA 0.852
            my $priorZetaString = `egrep MYREPL $priorOptDir\/$dirpreprod\/md.log`;
            my @priorZetaStringFields = split(/\s+/ , $priorZetaString);
            my $priorZeta = @priorZetaStringFields[5] + 0.0;
            $initalZetaVals[$localcounter] = $priorZeta;
            #print "command:  cp -a $priorOptDir\/$dirpreprod $dirpreprod \n";
            `mkdir $dirpreprod`;
         	`cp $priorOptDir\/$dirpreprod\/confout.gro $dirpreprod`;
         	`cp $priorOptDir\/$dirpreprod\/topol.top $dirpreprod`;
         	`cp $priorOptDir\/$dirpreprod\/state.cpt $dirpreprod`;
         	push @preprodsubdirs, $dirpreprod;
            $localcounter++;
         }
     }
     
     # at this point, @preprodsubdirs size should be the same as @temperatures size
     if (scalar @preprodsubdirs != scalar @temperatures) {
     	die "Something ELSE has gone horribly awry!!!";
     }
             
     for (my $i = 0; $i < scalar @preprodsubdirs; $i++) {
         # if we are in a non-scaled (normal) replica, myBeta == betaLeft == betaRight
         # if we are in a scaled replica (these will only have odd indices because of the gromacs does it)
         # then betaLeft will be the beta of my left neighbor and betaRight will be the beta of my right
         # neighbor
         my $rightReplicaIndex = 0;
         my $leftReplicaIndex = 0;
         if ( $i % 2 == 1 ) {
             $rightReplicaIndex = $i + 1;
             $leftReplicaIndex  = $i - 1;
             if ( ($i - 3) % 4 == 0 ) {      # special case where left and right are "flipped" CMS
                 $rightReplicaIndex = $i - 1;
                 $leftReplicaIndex = $i + 1;
             }
         } else {
             $rightReplicaIndex = $i;
             $leftReplicaIndex = $i;
         }

         my $betaLeft =  1.0 / ($temperatures[$leftReplicaIndex] * ((6.02214129e23 * 1.3806488e-23)/1000.0));
         my $betaRight =  1.0 / ($temperatures[$rightReplicaIndex] * ((6.02214129e23 * 1.3806488e-23)/1000.0));

         my $dir = @preprodsubdirs[$i];
         my $temp = @temperatures[$i];
         chomp $dir;
#         my @result12 = `./generate_REDS1_mdp.pl presim $temp $i $aVals[$i] $bVals[$i] $cVals[$i] $betaLeft $betaRight $initalZetaVals[$i] $zetaMaxStepVals[$i]; cd $dir; rm *.mdp; mv ../presim_$i.mdp ./presim.mdp; cd ..`;
         my @result12 = `./generate_REDS1_mdp.pl presim $temp $i $aVals[$i] $bVals[$i] $cVals[$i] $betaLeft $betaRight $initalZetaVals[$i] $zetaMaxStepVals[$i]; cd $dir; rm *.mdp; mv ../presim_$i.mdp ./presim.mdp; gmx grompp -f presim -c confout.gro -t state.cpt -p topol.top; cd ..`;
     }
     print "REDS1: BEGIN PREPRODUCTION RUN, ROUND $optimizationTrials\n";
     #this is the run step
     $mpiexecPrefix = "mpiexec -np $numTemps gmx_mpi mdrun -v -multidir ";
     $mpiexecSuffix = " -s topol.tpr -replex 100 -reseed $seed ";
     my $preprodsubdirlist = "";
     foreach $dir (@preprodsubdirs) {
     	chomp $dir;
     	$preprodsubdirlist = $preprodsubdirlist . $dir . " ";
     }
     print "$preprodsubdirlist\n";
     $fullCommand = $mpiexecPrefix . $preprodsubdirlist . $mpiexecSuffix;
     print "REDS1: Running : $fullCommand\n";
     my @result13 = `$fullCommand`;
     print @result13;
     print "REDS1: END PREPRODUCTION RUN, ROUND $optimizationTrials\n";
     # now, for each of the flattened (scaled) replicas, we need to do a curve fit
     # based on the <Epot> vs zeta - these replicas have indices 1,3,5,etc.
     for (my $repID = 1; $repID < $numTemps ; $repID++) {
        # odd replicas are flattened
        if ( $repID % 2 == 1) {
            my $repIDString = sprintf("%03i",$repID);
            my $outputfilename = "REDS" . $repIDString . ".txt";
            my @result135 = `cd $preprodsubdirs[$repID]; cp ../extractColumns.pl .; cat $outputfilename | grep -v nan > $outputfilename\_2; ./extractColumns.pl $outputfilename\_2 0 1 > temp1; cp ../histogram_and_average.pl .; ./histogram_and_average.pl temp1 0.1 -0.05 1.05 > temp2; ./extractColumns.pl $outputfilename\_2 2 > temp2.l; ./extractColumns.pl $outputfilename\_2 3 > temp2.r; echo "1.0 " > temp2.l2; cp ../average.pl .; ./average.pl temp2.l >> temp2.l2; echo "0.0 " > temp2.r2; ./average.pl temp2.r >> temp2.r2; awk -v col=2 ' { if ((cur + NF) < col) { printf(\$0); cur=cur+NF; } else { print \$0; cur=0; } }' temp2.l2 > temp2.l3; awk -v col=2 ' { if ((cur + NF) < col) { printf(\$0); cur=cur+NF; } else { print \$0; cur=0; } }' temp2.r2 > temp2.r3; cat temp2.l | wc -l > n.l ; paste -d" " temp2.l3 n.l > temp2.l4 ; cat temp2.r | wc -l > n.r ; paste -d" " temp2.r3 n.r > temp2.r4 ; cat temp2.r4 temp2 temp2.l4 > temp3 ; cp ../curveFitREDS.py .; $PYTHON3 curveFitREDS.py < temp3 &> out1; tail -1 out1 ; cp temp2 ../REDS_temp2\_rep$repIDString\_round$optTrialString\.txt;  cp $outputfilename ../REDS_output\_rep$repIDString\_round$optTrialString\.txt`;
            print "Replica: $repID ";
            print "After results135 optimization";
            print @result135;
            my $tempcurvefitresults = $result135[0];
            #print "tempcurvefitresults = $tempcurvefitresults";
            $tempcurvefitresults =~ s/(\]|\[)//g;
            #print "$tempcurvefitresults\n";
            my @curveFitValues = split(/\s+/ , $tempcurvefitresults);
            # now, before we modify the values of the biasing, check and see
            # what kind of distribution of zeta values this produced.  If the
            # distriubtion is roughly flat, then we're done, and we don't need
            # to do more fitting for this particular replica
            my $result35_5 = `cd $preprodsubdirs[$repID]; ./histogram_and_average.pl temp1 0.1 -0.05 1.05 > hist`; 
            my $result136 = `cd $preprodsubdirs[$repID]; cp ../compareHistogramToEvenDistribution.pl .; ./compareHistogramToEvenDistribution.pl hist 2 11`;
            chomp $result136;
            print "REDS_OPT: trial: $optimizationTrials replica: $repID diff from even distribution: $result136\n";
            #if ($result136 + 0.0 < 10.0) {
            if ($result136 + 0.0 < 15.0) {
                print "Don't need to fit $repID anymore!  $result136\n";
                $biasingOptimized[$repID] = 1;
            }
            else {
                if ($optimizationTrials == 0) {
                    # in the first round, the coefficients are replaced directly by the fit values
                    # in subsequent rounds, the new value is a mixture of the new value and old values
                    $aVals[$repID] = $curveFitValues[0];  
                    $bVals[$repID] = $curveFitValues[1];  
                    $cVals[$repID] = $curveFitValues[2];  
                } else {
                    # save the results of the curve fitting for the next round
                    $aVals[$repID] = ($aVals[$repID] + $curveFitValues[0])/2.0;  
                    $bVals[$repID] = ($bVals[$repID] + $curveFitValues[1])/2.0;  
                    $cVals[$repID] = ($cVals[$repID] + $curveFitValues[2])/2.0;  
                }
                print "Replica: $repID ";
                print "CURVEFIT:  $curveFitValues[0] $curveFitValues[1] $curveFitValues[2]\n";
                print "NEWFITVALS:  $aVals[$repID] $bVals[$repID] $cVals[$repID]\n";
            }
        }
     }
     my $optDir = "OPT_ROUND_" . $optTrialString;
     `mkdir $optDir; mv preprod* $optDir`;
     #this is how we'll clean up eventually
     #`rm -fr ./preprod*`; 
     print "aVals   bVals   cVals   at step $optimizationTrials\n";
     for (my $i=0; $i<$numTemps; $i++) {
        print "$aVals[$i] $bVals[$i] $cVals[$i]\n";
     }     
     my $biasSum = 0;
     for (my $repID = 0; $repID < $numTemps ; $repID++) {
             $biasSum += $biasingOptimized[$repID];
     }
     if ($biasSum == $numTemps) {
        $fullyBiasOptimized = 1;
     }
     $optimizationTrials++;
}
# end pre-production optimization loop
print("REDS1: END PREPRODCTION - PRODUCTION RUNS BEGIN");
#=pod
# ACTUAL PRODUCTION RUN FOLLOWS
# get set up for production run by copying equilibration directories
my @prodsubdirs;
my $localcounter2 = 0;
foreach $dir (@subdirs) {
	chomp $dir;
	my $dirprod = $dir;
	$dirprod =~ s/equil/prod/;
	my $dirpreprod = $dir;
	$dirpreprod =~ s/equil/preprod/;
    my $priorOptTrialString = sprintf("%03i",$optimizationTrials-1);
    my $priorOptDir = "OPT_ROUND_" . $priorOptTrialString;
    # the line we are looking for to grab the previous zeta value looks like:
    # !REDS MYREPL 0 : ZETA 0.852
    my $priorZetaString = `egrep MYREPL $priorOptDir\/$dirpreprod\/md.log`;
    my @priorZetaStringFields = split(/\s+/ , $priorZetaString);
    my $priorZeta = @priorZetaStringFields[5] + 0.0;
    $initalZetaVals[$localcounter2] = $priorZeta;
    #`cp -a $priorOptDir\/$dirpreprod $dirprod`;
    `mkdir $dirprod`;
    `cp $priorOptDir\/$dirpreprod\/confout.gro $dirprod`;
    `cp $priorOptDir\/$dirpreprod\/topol.top $dirprod`;
    `cp $priorOptDir\/$dirpreprod\/state.cpt $dirprod`;
	push @prodsubdirs, $dirprod;
    $localcounter2++;
}

# at this point, @prodsubdirs size should be the same as @temperatures size
if (scalar @prodsubdirs != scalar @temperatures) {
	die "Something has gone horribly awry!!!";
}

for (my $i = 0; $i < scalar @prodsubdirs; $i++) {
    # if we are in a non-scaled (normal) replica, myBeta == betaLeft == betaRight
    # if we are in a scaled replica (these will only have odd indices because of the gromacs does it)
    # then betaLeft will be the beta of my left neighbor and betaRight will be the beta of my right
    # neighbor
    my $rightReplicaIndex = 0;
    my $leftReplicaIndex = 0;
    if ( $i % 2 == 1 ) {
        $rightReplicaIndex = $i + 1;
        $leftReplicaIndex  = $i - 1;
        if ( ($i - 3) % 4 == 0 ) {      # special case where left and right are "flipped" CMS
            $rightReplicaIndex = $i - 1;
            $leftReplicaIndex = $i + 1;
        }
    } else {
        $rightReplicaIndex = $i;
        $leftReplicaIndex = $i;
    }

    my $betaLeft =  1.0 / ($temperatures[$leftReplicaIndex] * ((6.02214129e23 * 1.3806488e-23)/1000.0));
    my $betaRight =  1.0 / ($temperatures[$rightReplicaIndex] * ((6.02214129e23 * 1.3806488e-23)/1000.0));
    my $dir = @prodsubdirs[$i];
    my $temp = @temperatures[$i];
    chomp $dir;
    my @result14 = `./generate_REDS1_mdp.pl sim $temp $i $aVals[$i] $bVals[$i] $cVals[$i] $betaLeft $betaRight $initalZetaVals[$i] $zetaMaxStepVals[$i]; cd $dir; rm *.mdp; mv ../sim_$i.mdp ./sim.mdp; gmx grompp -f sim -c confout.gro -t state.cpt -p topol.top; cd ..`;
}


print "REDS1: BEGIN PRODUCTION RUN\n";
#this is the run step
#my @result15 = `mpiexec -np 6 gmx_mpi mdrun -v -multidir prod[012345] -s topol.tpr -replex 100 -reseed 175320 -REDS FALSE`;
$mpiexecPrefix = "mpiexec -np $numTemps gmx_mpi mdrun -v -multidir ";
$mpiexecSuffix = " -s topol.tpr -replex 100 -reseed $seed";
my $prodsubdirlist = "";
foreach $dir (@prodsubdirs) {
	chomp $dir;
	$prodsubdirlist = $prodsubdirlist . $dir . " ";
}
print "$prodsubdirlist\n";
$fullCommand = $mpiexecPrefix . $prodsubdirlist . $mpiexecSuffix;
my @result15 = `$fullCommand`;
print @result15;
print "REDS1: END PRODUCTION RUN\n";
#=cut
