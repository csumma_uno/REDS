#!/usr/bin/perl -w

use strict;

if ( scalar @ARGV != 1) {
    die "usage: average.pl filename";
}

my $filename = $ARGV[0];
my $THEFILE;

my @xvals;
open ($THEFILE,$filename) or die "Couldn't open file $filename\n";
while (<$THEFILE>)
{
	chomp;
    my $templine = $_;
    my @fields = split(/\s+/,$_);
    my $x = $fields[0] + 0.0;
    push @xvals,$x;
}
close $THEFILE;

my $average = 0.0;
for (my $i=0; $i< @xvals+0; $i++) {
    $average = $average + $xvals[$i];
}
$average = $average / (@xvals + 0);
print "$average\n";
