#!/usr/bin/perl

use strict;
my $filename = shift(@ARGV);
my $numArgumentsLeft = scalar @ARGV;
#print "numArgumentsLeft = $numArgumentsLeft\n";
my @columnsToExtract;
for (my $i=0; $i<$numArgumentsLeft; $i++) {
    push @columnsToExtract, shift(@ARGV)
}

#print "@columnsToExtract\n";

my $FILE;
open ($FILE,$filename) or die "Couldn't open file $filename\n";
while (<$FILE>)
{
    my @fields = split(/\s+/, $_);
    for (my $i=0; $i<@columnsToExtract+0; $i++) {
        if ($i != 0) {
            print " ";
        }
        print "$fields[$columnsToExtract[$i]]";
    }
    print "\n";
}

