#!/usr/bin/perl

use strict;
use warnings;

if (@ARGV + 0 != 5) {
	die("Usage: generate_REDS1_temps.pl tempLow tempHigh #ProtAtoms #Waters #skip\n");
}
my $tempLow = shift @ARGV;
my $tempHigh = shift @ARGV;
my $Np = shift @ARGV;
#print "Num prot: $Np\n";
my $Nw = shift @ARGV;
#print "Num water: $Nw\n";
my $skip = shift @ARGV;
if ($skip < 1) {
	die "ERROR: At this point generate_REDS1_temps.pl only accepts values of replicas to skip greater than or equal to 1. Please choose different values\n";
}

my $success = 0;
my $maxTrials = 10;
my $numTrials = 0;
     
my $output = `./tgenerator.pl --low $tempLow --high $tempHigh --prob 0.5 --nwat $Nw --nprot $Np`;
#my $output = capture($^X, "tgenerator.pl", $tempLow, $tempHigh, 0.5, $Nw, $Np);
     
     #print $output;
     
     #print "got the output\n";
     my @words = split /\s+/, $output;
     my @temps;
     my $numWords = scalar @words;
     #print "numWords: $numWords\n"; 
     for(my $i = 0; $i < scalar @words; $i++){
     	if ($words[$i] eq 'Enjoy.'){
             until ($words[$i++] eq 'If'){
     		    $words[$i] =~ s/,//ig;
		        if ($words[$i] ne 'If') {
     		        push @temps, $words[$i];
		        }
             }
     	 }
     }

     my $numTemps = @temps + 0;
     my @modifiedTemps;
     # In the REDS1 implementation, we want the first replica to be a "normal" one, as well as the last one.
     # the general form of the if - else statement below, as we've defined skip would be:
     # if ( ($numTemps >= 3+(2*$skip)) &&  ($numTemps-(3+(2*$skip)) % ((2*$skip)+2) == 0 ) {
     #     for (my $i=0; $i<$numTemps; $i += $skip+1) {
     #         push @modifiedTemps, $temps[$i]
     #     }
     #     push @modifiedTemps, $temps[$numTemps-1];
     #     $success = 1;
     # }

     #foreach (@temps) {
	 #   print "$_ ";
     #}
     #print "\n";

     if ( ($numTemps >= 3+(2*$skip)) &&  (($numTemps-(3+(2*$skip))) % ((2*$skip)+2) == 0 )) {
         for (my $i=0; $i<$numTemps; $i += $skip+1) {
             push @modifiedTemps, $temps[$i];
         }
         $success = 1;
     } else {
         # if we've hit this point, then the number of temps does not exactly fit our constraints.
         # just go as far as we can, and tack on the last temperature
         for (my $i=0; $i<$numTemps-1; $i += $skip+1) {
             push @modifiedTemps, $temps[$i]
         }
         push @modifiedTemps, $temps[$numTemps-1];   
         $success = 1;
     }

     if ($numTrials >= $maxTrials && $success == 0) {
         die "ERROR: Unable to find a reasonable set of replica temps for the temperature ranges\nand replica skipping values chosen.  Please choose different values\n";
     }
     # REMOVE OR MODIFY THIS PRINT STATEMENT UNDER PENALTY OF DEATH (DR. SUMMA)
     if ($success == 1) {
        foreach (@modifiedTemps) {
		   print "$_ ";
        }
     }

