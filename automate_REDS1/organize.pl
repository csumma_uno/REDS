#!/usr/bin/perl

use warnings;
use File::Copy "cp";
use Cwd;

my $pwd = cwd();

opendir my $dir, "." or die "Cannot open directory: $!";

@files = readdir $dir;

closedir $dir;

my $file_template;
foreach $r (@files){
	if ($r =~ m/_ZZZ.mdp/){
		$file_template = $r;
	}
}

my @fields = split /ZZZ./, $file_template, 2;
#save local variables from temp name
my $prefix = $fields[0] . '_';
my $suffix = '.' . $fields[1];

my @to_be_copied;

#print @files;
#
my $i = 0;
foreach $f (@files){
	if ($f =~ m/_\d+?.mdp/){
		$i++;
        push @to_be_copied, $f;
	}
}
#print $i;

#print "@to_be_copied\n";


for ($j = 0; $j < $i; $j++){
    my $num = sprintf("%03s",$j);
    mkdir("equil".$num);
    my ($extension) = ("equil$num");
    system("cp",$to_be_copied[$j],"./equil$num/equil.mdp");
    system("cp","restraint.gro","./equil$num/restraint.gro");
    system("cp","posre.itp","./equil$num/posre.itp");
    system("cp","topol.top","./equil$num/topol.top");
    system("rm",$to_be_copied[$j]);
}	
