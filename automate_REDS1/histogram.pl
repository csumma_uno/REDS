#!/usr/bin/perl -w

use strict;

my $filename = $ARGV[0];
my $stepstring = $ARGV[1];
my $userminstring = $ARGV[2];
my $usermaxstring = $ARGV[3];
my $THEFILE;

my $step;
if ($stepstring)
{
	$step = $stepstring + 0.0;
}
else
{
	$step = 1.0;
}

my $min = 10000000.0;
my $max = -10000000.0;

my @xvals;
open ($THEFILE,$filename) or die "Couldn't open file $filename\n";
while (<$THEFILE>)
{
	chomp;
    my $templine = $_;
    my @fields = split(/\s+/,$_);
    my $x = $fields[0] + 0.0;
    push @xvals,$x;
    if ($x < $min)
    {
		$min = $x;
	}
	if ($x > $max)
	{
		$max = $x;
	}
}
close $THEFILE;

$min  -= $step * 2.0;
$max  += $step * 2.0;

if ($userminstring)
{
	$min = $userminstring + 0.0;
}
if ($usermaxstring)
{
	$max = $usermaxstring + 0.0;
}

#print "$step $min $max\n";
my @binvals;
my @maxvals;
my @meanvals;
my $numbinvals =  (($max - $min)/$step) + 1;
for (my $i=0; $i<$numbinvals; $i++)
{
	push @binvals, 0;
	push @maxvals, $min + ($i * $step);
	push @meanvals, $min + ($i * $step) - $step/2.0;
}

#my $temp =  @binvals + 0;
#print "$numbinvals  $temp\n";


for (my $i=0; $i<@xvals+0; $i++)
{
	my $val = $xvals[$i];
#	print "$val\n";
	for (my $j = $numbinvals-1; $j>=0; $j--)
	{
#		print "\t\t$maxvals[$j]\n";
		if ($val > $maxvals[$j] && $j+1 < $numbinvals)
		{
			$binvals[$j+1]++;
			last;
		}
	}
}


for (my $i=0; $i<$numbinvals; $i++)
{
	#print "$i  $binvals[$i]   $meanvals[$i]    $maxvals[$i]\n";
	print "$meanvals[$i]  $binvals[$i]\n";
}




#for (my $i=0; $i<@xvals+0; $i++)
#{
#}

