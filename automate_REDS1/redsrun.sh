nn="00"

for dir in prod0*
 do cd $dir
  gmx grompp -f sim -c confin.gro -p topol.top -maxwarn 3
  cp confin.gro confin_0.gro
  cp sim.mdp sim0.mdp
 cd ..
done

srun -n 7 gmx_mpi mdrun -v -multidir  ./prod000 ./prod001 ./prod002 ./prod003 ./prod004 ./prod005 ./prod006 -s topol.tpr -replex 10 -reseed 8252

for i in $(ls | grep "prod*"); do
  rem="$((10#${i: -3}%2))"
  cd $i
   if [ $rem == 1 ]
    then
#   for scaled replicas
#   update zeta
    sh redsupdatezeta.sh
    mv REDS*.txt /work/srick/hIAPP/$i/REDS_$nn.txt
    cat /work/srick/hIAPP/$i/REDS_*.txt >> REDS.all
#   update abc parameters using all the REDS data
    sh redsupdateabc.sh
   fi
# save output files to /work and prepare for next run
   mv traj.trr  /work/srick/hIAPP/$i/traj_$nn.trr
#  plus any other files we want to save
   mv confout.gro confin.gro
   rm REDS* *edr* *md.log*
  cd ..
done


The COLAEHD Grievance Committee representative will be Dr. Andrea Mosterman, Associate Professor and Joseph Tregle Professor in Early American History, Department of History and Philosophy. The alternate representative will be Francis O. Adeola, Ph.D.
Professor of Sociology.

