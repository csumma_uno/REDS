The Python parts of these automated scripts depend on the following being installed:

python (some 3.x version)
numpy
scipy

# the final argument (tempDirectory) is optional
./A1-run-it-all.pl ala.pdb 300 400 3 3445 tempDirectory >& output.txt &

# the final argument (tempDirectory) is optional
./A1-run-it-all.py ala.pdb 300 400 3 3445 tempDirectory >& output.txt &
