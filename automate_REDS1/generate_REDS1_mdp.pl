#!/usr/bin/perl

###############################################################
################	USAGE	###############################
#	perl generate_mdp.pl mdpTemplatePrefx temperature index
#	arg1 :a filename prefix fitting *_ZZZ.mdp (equil or prod)
#	arg2 :the temperatures to replace ZZZ with
#	arg3 :the index of this replica
#	arg4 : biasing "a" value
#	arg5 : biasing "b" value
#	arg6 : biasing "c" value
#   arg7 : beta of left replica
#   arg8 : beta of right replica
#
###############################################################	

#use strict;
#use warnings;
#use diagnostics;

use File::Copy "cp";

#print "The arguments before 1 shift: @ARGV\n";

my $prefix = shift(@ARGV);
my $temperature = shift (@ARGV);
my $index = shift (@ARGV);
my $a     = shift (@ARGV);
my $b     = shift (@ARGV);
my $c     = shift (@ARGV);
my $betaLeft = shift (@ARGV);
my $betaRight = shift (@ARGV);
my $zetaInit  = shift (@ARGV);
my $zetaMaxStep = shift (@ARGV);

# generate new name
my $new_file = $prefix . "_" . $index . ".mdp";	
my $file_template = $prefix . "_" . "ZZZ.mdp";

#print $new_file;	
cp($file_template, $new_file) or die ("copy $new_file failed");
	
# Replace instances of ZZZ with the temp
`perl -pi -e 's/ZZZ/$temperature/g' $new_file`;

# add lines to the end with parameters for REDS1 cubic biasing potential equation
`echo 'a_reds     =    $a     ; for REDS1 replica exchange biasing potential' >> $new_file`;
`echo 'b_reds     =    $b     ; for REDS1 replica exchange biasing potential' >> $new_file`;
`echo 'c_reds     =    $c     ; for REDS1 replica exchange biasing potential\n' >> $new_file`;
`echo 'betaLeft_reds     =    $betaLeft     ; for REDS1 replica exchange scaling\n' >> $new_file`;
`echo 'betaRight_reds     =    $betaRight     ; for REDS1 replica exchange scaling\n' >> $new_file`;
`echo 'zeta_init_reds     =    $zetaInit    ; for REDS1 replica exchange scaling\n' >> $new_file`;
`echo 'zeta_maxstep_reds     =    $zetaMaxStep     ; for REDS1 replica exchange scaling\n' >> $new_file`;
