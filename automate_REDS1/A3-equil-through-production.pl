#!/usr/bin/perl

if (scalar @ARGV != 4) {
        die ("Usage: A3-equil-through-production.pl lowTemp highTemp replica_skip seed\n");
}

my $lowTemp = shift @ARGV;
my $highTemp = shift @ARGV;
my $skip     = shift @ARGV;
my $seed    = shift @ARGV;

my $PYTHON3 = "python3";
#my $PYTHON3 = "python3.6";

# THIS IS THE POINT WHERE WE SHOULD SPLIT THIS INTO TWO SCRIPTS.
# EVERYTHING PRIOR TO THIS POINT IS GETTING THE FILES SET UP FOR
# THE USER'S SPECIFIC SYSTEM.

my @result8 = `cp em.gro restraint.gro`;

print @result8;

my @result9 = `./automate_REDS1.pl $lowTemp $highTemp $skip`;

if (@result9[scalar @result9 -1] =~ /ERROR/) {
        die "$result9[scalar @result9 -1]\n";
}

print @result9;

my $rawTemperatureString = @result9[@result9-1];
print "REDS1: I think the temperature string is: $rawTemperatureString\n";
my @temperatureOutput = split /\s+/ , $rawTemperatureString;
my $numTemps = scalar @temperatureOutput;
print "REDS1: numtemps = $numTemps\n";
my @temperatures;
for (my $i=0; $i < scalar @temperatureOutput; $i++) {
        push @temperatures, @temperatureOutput[$i]
}

if ($numTemps % 2 != 1) {
        die "Number of replicas (i.e. temps) should be odd.  Choose different temp range\n";
}

my @subdirs_unsorted = `find . -type d | grep equil`;

for (my $i = 0; $i<@subdirs+0; $i++) {
   $subdirs_unsorted[$i] =~ s/\.\///;
}

my @subdirs = sort @subdirs_unsorted;

foreach $dir (@subdirs) {
        chomp $dir;
        #print $dir;
        my @result10 = `cd $dir; gmx grompp -f equil -c restraint.gro -p topol.top; cd ..`;
        print @result10;
}
#this is going to run the equilibration step
print "REDS1: BEGIN EQUILIBRATION RUN\n";
my $mpiexecPrefix = "mpiexec -n $numTemps gmx_mpi mdrun -v -multidir ";
my $mpiexecSuffix = " -s topol.tpr -reseed $seed ";
my $subdirlist = "";
foreach $dir (@subdirs) {
        chomp $dir;
        $subdirlist = $subdirlist . $dir . " ";
}
print "REDS1: subdirlist is: $subdirlist\n";
my $fullCommand = $mpiexecPrefix . $subdirlist . $mpiexecSuffix;
print "REDS!: full command is: $fullCommand\n";
my @result11 = `$fullCommand`;
print @result11;
print "REDS1: END EQUILIBRATION RUN\n";
#done equilibrating
#
# begin setup for pre-production optimization loop
# setup arrays for a,b,c biasing constants - one value per replica
my @aVals;
my @bVals;
my @cVals;
for (my $i=0; $i < $numTemps; $i++) {
    push @aVals, -500000.0;
    push @bVals, 500000.0;
    push @cVals, 0.0;
}

#set up biasing parameters for the unflattened replicas - these will not be optimized
for (my $repID=0; $repID < $numTemps; $repID++) {
    if ( ($repID % 4) - 2 == 0 ) {
        print "Replica: $repID should have zeta near 0\n";
        # this replica should have a zeta value that tends toward 0
        $aVals[$repID] = 0;
        $bVals[$repID] = 250;
        $cVals[$repID] = 0;
    }
    elsif ( $repID % 4 == 0 ) {
        print "Replica: $repID should have zeta near 1\n";
        # this replica should have a zeta value that tends toward 1
        $aVals[$repID] = -500;
        $bVals[$repID] = 250;
        $cVals[$repID] = 0;
    }
}

# code for setting up data structures to hold initial zeta values and zeta maxstep values
my @initalZetaVals;
my @zetaMaxStepVals;
for (my $repID = 0; $repID < $numTemps ; $repID++) {
    if ( $repID % 2 == 1 )
    {
         push @initalZetaVals, 0.5;
         push @zetaMaxStepVals, 0.1;
    }
    elsif ( ($repID % 4) -2 == 0 )
    {
         push @initalZetaVals, 0.1;
         push @zetaMaxStepVals, 0.1;
    }
    elsif ($repID % 4 == 0)
    {
         push @initalZetaVals, 0.9;
         push @zetaMaxStepVals, 0.1;
    }
      else
    {
         push @initalZetaVals, 0.0;
         push @zetaMaxStepVals, 0.1;
    }
}
#=pod
# ACTUAL PRODUCTION RUN FOLLOWS
# get set up for production run by copying equilibration directories
my @prodsubdirs;
my $localcounter2 = 0;
foreach $dir (@subdirs) {
        chomp $dir;
        my $dirprod = $dir;
        $dirprod =~ s/equil/prod/;
    `mkdir $dirprod`;
    `cp $dir\/confout.gro $dirprod`;
    `cp $dir\/topol.top $dirprod`;
    `cp $dir\/state.cpt $dirprod`;
        push @prodsubdirs, $dirprod;
    $localcounter2++;
}

# at this point, @prodsubdirs size should be the same as @temperatures size
if (scalar @prodsubdirs != scalar @temperatures) {
        die "Something has gone horribly awry!!!";
}

for (my $i = 0; $i < scalar @prodsubdirs; $i++) {
    # if we are in a non-scaled (normal) replica, myBeta == betaLeft == betaRight
    # if we are in a scaled replica (these will only have odd indices because of the gromacs does it)
    # then betaLeft will be the beta of my left neighbor and betaRight will be the beta of my right
    # neighbor
    my $rightReplicaIndex = 0;
    my $leftReplicaIndex = 0;
    if ( $i % 2 == 1 ) {
        $rightReplicaIndex = $i + 1;
        $leftReplicaIndex  = $i - 1;
        if ( ($i - 3) % 4 == 0 ) {      # special case where left and right are "flipped" CMS
            $rightReplicaIndex = $i - 1;
            $leftReplicaIndex = $i + 1;
        }
    } else {
        $rightReplicaIndex = $i;
        $leftReplicaIndex = $i;
    }

    my $betaLeft =  1.0 / ($temperatures[$leftReplicaIndex] * ((6.02214129e23 * 1.3806488e-23)/1000.0));
    my $betaRight =  1.0 / ($temperatures[$rightReplicaIndex] * ((6.02214129e23 * 1.3806488e-23)/1000.0));
    my $dir = @prodsubdirs[$i];
    my $temp = @temperatures[$i];
    chomp $dir;
    my @result14 = `./generate_REDS1_mdp.pl sim $temp $i $aVals[$i] $bVals[$i] $cVals[$i] $betaLeft $betaRight $initalZetaVals[$i] $zetaMaxStepVals[$i]; cd $dir; rm *.mdp; mv ../sim_$i.mdp ./sim.mdp; gmx grompp -f sim -c confout.gro -t state.cpt -p topol.top; cd ..`;
}


print "REDS1: BEGIN PRODUCTION RUN\n";
#this is the run step
#my @result15 = `mpiexec -np 6 gmx_mpi mdrun -v -multidir prod[012345] -s topol.tpr -replex 100 -reseed 175320 -REDS FALSE`;
$mpiexecPrefix = "mpiexec -n $numTemps gmx_mpi mdrun -v -multidir ";
$mpiexecSuffix = " -s topol.tpr -replex 100 -reseed $seed";
my $prodsubdirlist = "";
foreach $dir (@prodsubdirs) {
        chomp $dir;
        $prodsubdirlist = $prodsubdirlist . $dir . " ";
}
print "$prodsubdirlist\n";
$fullCommand = $mpiexecPrefix . $prodsubdirlist . $mpiexecSuffix;
my @result15 = `$fullCommand`;
print @result15;
print "REDS1: END PRODUCTION RUN\n";
#=cut
