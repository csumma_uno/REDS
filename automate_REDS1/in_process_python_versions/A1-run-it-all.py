#!/usr/bin/python
import sys
import subprocess

if (len(sys.argv) != 6 | len(sys.argv) != 7):#and len(sys.argv != 6)):
	sys.exit("Usage: Python a1v1 pdbname lowTemp highTemp replica_skip seed directory_to_create_and_pipe_output") #if (scalar @ARGV != 5 && scalar @ARGV != 6) {
        #die ("Usage: A1-run-it-all.pl pdbname lowTemp highTemp replica_skip seed directory_to_create_and_pipe_output \n");
#}

dir_name = "./temp"
if (len(sys.argv) == 7):
	dir_name = sys.argv[6]


#in a Python script, the name of the script itself is the "argument" at position 0
pdbname = sys.argv[1] #my $pdbname = shift @ARGV;
lowTemp = sys.argv[2] #my $lowTemp = shift @ARGV;
highTemp = sys.argv[3] #my $highTemp = shift @ARGV;
skip = sys.argv[4] #my $skip     = shift @ARGV;
seed = sys.argv[5] #my $seed    = shift @ARGV;
#dir_name = sys.argv[6] #my $dir_name    = shift @ARGV;


subprocess.call(['mkdir', dir_name]) #system("mkdir $dir_name");
subprocess.call(['touch', 'timestamp1']) #system("touch timestamp1");

result1 = subprocess.check_output(['./A2-pdb-up-to-equil.py', pdbname]).split() #my @result1 = `./A2-pdb-up-to-equil.pl $pdbname`;

print(result1) #print @result1;

result2 = subprocess.check_output(['./A3-equil-through-production.pl', lowTemp, highTemp ,skip, seed]).split() #my @result2 = `./A3-equil-through-production.pl $lowTemp $highTemp $skip $seed`;

print(result2) #print @result2;

subprocess.call(['sleep 1m']) #system('sleep 1m');
subprocess.call(['touch', 'timestamp2']) #system("touch timestamp2");

longCMDList = ['find', '.', '-newer','timestamp1', '!', '-newer', 'timestamp2', '|', 'xargs', 'tar', '--no-recursion', '-czf',dir_name + '/' + dir_name + '/' + dir_name + '.tgz']

subprocess.call(longCMDList) #system("find . -newer timestamp1 ! -newer timestamp2 | xargs tar --no-recursion -czf $dir_name/$dir_name.tgz");
subprocess.call(['./cleanup.sh']) #system('./cleanup.sh');
subprocess.call(['rm','timestamp']) #system('rm timestamp*');






print("SUCCESS!!!")
