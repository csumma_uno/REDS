#!/usr/bin/python
import sys
import subprocess
import os
if (len(sys.argv) != 2):#if (scalar @ARGV != 1) {
	sys.exit("Usage: A2-pdb-up-to-equil.pl pdbname\n")#        die ("Usage: A2-pdb-up-to-equil.pl pdbname\n");
#}

pdbname = sys.argv[1]#my $pdbname = shift @ARGV;

#result1 = subprocess.check_output(['echo', '15', 'echo', '3'])

#the os.system funtion plays nicer with the echo commands
result1 = os.system('(echo 15; echo 3; echo 3) | gmx pdb2gmx -f ala.pdb -o sys_processed.gro -water spce -ter')#my @result1 = `(echo 15; echo 3; echo 3) | gmx pdb2gmx -f $pdbname -o sys_processed.gro -water spce -ter`;


print(result1)#print @result1;

result2 = subprocess.call(['gmx', 'editconf', '-f', 'sys_processed.gro', '-o', 'sys_newbox.gro','-c', '-d', '1.0', '-bt', 'cubic'])#my @result2 = `gmx editconf -f sys_processed.gro -o sys_newbox.gro -c -d 1.0 -bt cubic`;

print(result2)#print @result2;

result3 = subprocess.call(['gmx', 'solvate', '-cp', 'sys_newbox.gro', '-cs', 'spc216.gro', '-o', 'sys_solv.gro', '-p', 'topol.top'])#my @result3 = `gmx solvate -cp sys_newbox.gro -cs spc216.gro -o sys_solv.gro -p topol.top`;

print(result3)#print @result3;

result4 = subprocess.call(['gmx', 'grompp', '-f', 'ions.mdp', '-c', 'sys_solv.gro','-p', 'topol.top','-o','ions.tpr', '--maxwarn', '1'])#my @result4 = `gmx grompp -f ions.mdp -c sys_solv.gro -p topol.top -o ions.tpr --maxwarn 1`;


print(result4)#print @result4;

#This is where ions would be added, if you need to add them.
#TODO write some code to parse output above, and add the ions

result5 = subprocess.call(['cp', 'sys_solv.gro', 'sys_solv_ions.gro']) #my @result5 = `cp sys_solv.gro sys_solv_ions.gro`;


print(result5)#print @result5;

result6 = subprocess.call(['gmx', 'grompp','-f', 'minim.mdp', '-c','sys_solv_ions.gro', '-p', 'topol.top', '-o', 'em.tpr']) #my @result6 = `gmx grompp -f minim.mdp -c sys_solv_ions.gro -p topol.top -o em.tpr`;


print(result6)#print @result6;

result7 = subprocess.call(['gmx', 'mdrun', '-v', '-deffnm','em']) #my @result7 = `gmx mdrun -v -deffnm em`;

print(result7)#print @result7;

