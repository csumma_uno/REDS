#!/usr/bin/python

import sys
import subprocess
import os
#if (scalar @ARGV != 4) {
#        die ("Usage: A3-equil-through-production.pl lowTemp highTemp replica_skip seed\n");
#}
if(len(sys.argv) != 5):
	sys.exit("Usage: A3-equil-through-production.pl lowtemp hightemp replica_skip seed\n")

lowtemp = sys.argv[1]#my $lowTemp = shift @ARGV;
hightemp = sys.argv[2]#my $highTemp = shift @ARGV;
skip = sys.argv[3]#my $skip     = shift @ARGV;
seed = sys.argv[4]#my $seed    = shift @ARGV;

# THIS IS THE POINT WHERE WE SHOULD SPLIT THIS INTO TWO SCRIPTS.
# EVERYTHING PRIOR TO THIS POINT IS GETTING THE FILES SET UP FOR
# THE USER'S SPECIFIC SYSTEM.

result8 = subprocess.check_output(['cp', 'em.gro', 'restraint.gro'])#my @result8 = `cp em.gro restraint.gro`;

print(result8)#print @result8;

result9 = subprocess.check_output(['./automate_REDS1.pl', lowtemp, hightemp, skip]).split()#my @result9 = `./automate_REDS1.pl $lowTemp $highTemp $skip`;


print(result9)#print @result9;

################################################################################
#if (@result9[scalar @result9 -1] =~ /ERROR/) {
#        die "$result9[scalar @result9 -1]\n";
#}
for i in range(len(result9) - 1):
	if result9[i] == 'ERROR':
		sys.exit(result9[i] + '\n')

######################## HARD CODED HARD CODED HARD CODED HARD CODED#################
#These next few lines will take the last 5 values from 
#result9 and add them into a list for further use throughout
rawTemperatureString = []#creates an empty list for the temperature string
last = len(result9) - 5#we are only interested in the last 5 list elements
for i in range((last), len(result9)):#iteirate through the list and appends the values to a new list called rawTemperatureString
	rawTemperatureString.append(int(result9[last]))
	last += 1

#print "I think the temperature string is: $rawTemperatureString\n";
print("I think the temperature string is: ")
print(rawTemperatureString)

temperatureOutput = rawTemperatureString
numtemps = len(temperatureOutput)
print("numtemps = " + str(numtemps))

temperatures = []
for i in range(len(temperatureOutput)):
	temperatures.append(temperatureOutput[i])

if numtemps % 2 != 1:
	sys.exit("Number of replicas (i.e. temps) should be odd. Choose a different temp ranage\n")

subdirs_unsorted = subprocess.check_output('find . -type d | grep equil', shell=True)
subdirs = subdirs_unsorted.split()
subdirs.sort()
print(subdirs)

result10 = []
for i in range(len(subdirs)):
	cmdString = 'cd ' + subdirs[i] + '; gmx grompp -f equil -c restraint.gro -p topol.top; cd ..'
	result10.append(subprocess.check_output(cmdString, shell=True))
	print(result10)

print('REDS1: BEGIN EQUILIBRATION RUN\n')

mpiexecPrefix = 'mpiexec -np ' + str(numtemps) + ' gmx_mpi mdrun -v -multidir '
mpiexecSuffix = ' -s topol.tpr -reseed ' + str(seed)
subdirlist = ""

for i in range(len(subdirs)):
	subdirlist = subdirlist + subdirs[i] + ' '

fullcommand = mpiexecPrefix + subdirlist + mpiexecSuffix
result11 = subprocess.check_output(fullcommand, shell=True)
print(result11)
print('REDS1: END EQUILIBRATION RUN\n')
#done equilibrating
#
#begin setup for pre-production optimization loop
#setup arrays for a,b,c biasing constants - one value per replica
avals = []
bvals = []
cvals = []

for i in range(numtemps):
	avals.append(0.0)
	bvals.append(0.0)
	cvals.append(0.0)

for repID in range(numtemps):
	if((repID % 4) - 2 == 0):
		print("Rplica: " + str(repID) + " should have zeta near 0\n")
		#this replica should have a zeta value that tends toward 0
		avals[repID] = 0
		bvals[repID] = 250
		cvals[repID] = 0
	elif(repID % 4 == 0):
		print("Replica: " + str(repID) + " should have a zeta near 1\n")
		#this replica should have a zeta value that tends toward 1
		avals[repID] = -500
		bvals[repID] = 250
		cvals[repID] = 0

#set up a data structure so we can track when the flattened replicas
#have sufficiently optimized biasing potentials
biasingOptimized = []
#set up biasing parameters for the unflattened replicas - these will not be optimized
for repID in range(numtemps):
	if repID % 2 == 1:
		biasingOptimized.append(0)#flattens odd replicas
	else:
		biasingOptimized.append(1)

fullyBiasOptimized = 0;

#begin pre-production optimization loop for biasing parameters of flattened replicas
optimizationTrials = 0;

while (not(optimizationTrials > 8) and not(fullyBiasOptimized == 1)):

	#get set up for prepriduction run by copying equilibration directories
	preprodsubdirs = []
	for dirs in range(len(subdirs)):
		dirpreprod = subdirs[dirs].replace('equil','preprod')
		subprocess.call('cp -a ' + subdirs[dirs] + ' ' + str(dirpreprod), shell=True)
		preprodsubdirs.append(dirpreprod)
		#dirpreprod #line 119 TO BE COMPLETED
		#cp -a dir dirprepod #line 120 TO BE COMPLETED
	
	if (len(preprodsubdirs)) != (len(temperatures)):
		print(str(len(preprodsubdirs)) + ' ' + str(len(temperatures)) + '\n')
		sys.exit("Something ELSE has gone horribly awry!!!")
	
	for i in range(len(preprodsubdirs)):
		dir_ = preprodsubdirs[0]
		temp = temperatures[0]
		result12 = subprocess.check_output('./generate_REDS1_mdp.pl presim ' + str(temp) + ' ' +  str(0) + ' ' + str(avals[0]) + ' ' + str(bvals[0]) + ' ' + str(cvals[0]) + '; cd ' + dir_ + '; rm *mdp; mv ../presim_' + str(0) + '.mdp ./presim.mdp; gmx grompp -f presim -c confout.gro -t state.cpt -p topol.top; cd ..', shell=True)
		
	print('REDS1: BEGIN PREPRODUCTION RUN, ROUND ' + str(optimizationTrials))
	
	mpiexecPrefix = 'mpiexec -np ' + str(numtemps) + ' gmx_mpi mdrun -v -multidir '
	mpiexecSuffix = ' -s topol.tpr -replex 100 -reseed ' + str(seed)
	
	preprodsubdirlist = ''

	for dir_ in range(len(preprodsubdirs)):
		preprodsubdirlist = preprodsubdirlist + str(preprodsubdirs[dir_]) + ' '
	
	print(preprodsubdirlist)
	
	fullcommand = mpiexecPrefix + preprodsubdirlist + mpiexecSuffix 
	result13 = subprocess.check_output(fullcommand, shell = True)
	print('I printed this!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
	print(result13)
	print('REDS1: END PREPRODUCTION RUN, ROUND ' + str(optimizationTrials) + '\n' )

	#now, for each of the flattened (scaled) replicas, we need to do a curve fit
	#bsed on the <Epot> vs zeta - these replicas have indicies 1,3,5,etc.
	form = str('000{}')
	optTrialString = form.format(optimizationTrials) 
	print(optTrialString)#print to check output
	
	for repID in range(1,numtemps):
		if((repID + 1) % 2 == 1):
			repIDString = form.format(repID + 1)
			outputfilename = str('REDS' + repIDString + '.txt')
	result135 = subprocess.check_output('cd ' + preprodsubdirs[repID] + '; cp ../extractColumns.pl .; cat ' + outputfilename + ' | grep -v nan > ' + outputfilename + '\_2; ./extractColumns.pl ' + outputfilename + '\_2 0 1 > temp1; cp ../histogram_and_average.pl .; ./histogram_and_average.pl temp1 0.1 0.05 0.95 > temp2; ./extractColumns.pl ' + outputfilename + '\_2 2 > temp2.l; ./extractColumns.pl $outputfilename\_2 3 > temp2.r; echo "1.0 " > temp2.l2; cp ../average.pl .; ./average.pl temp2.l >> temp2.l2; echo "0.0 " > temp2.r2; ./average.pl temp2.r >> temp2.r2; awk -v col=2  { if ((cur + NF) < col) { printf(\$0); cur=cur+NF; } else { print \$0; cur=0; } } temp2.l2 > temp2.l3; awk -v col=2  { if ((cur + NF) < col) { printf(\$0); cur=cur+NF; } else { print \$0; cur=0; } } temp2.r2 > temp2.r3; cat temp2.r3 > temp3; ./extractColumns.pl temp2 0 1 >> temp3; cat temp2.l3 >> temp3; cp ../curveFitREDS.py .; python3.6 curveFitREDS.py < temp3; cp temp2 ../REDS_temp2\_rep' + repIDString + '\_round$optTrialString\.txt;  cp ' + outputfilename + ' ../REDS_output\_rep' + repIDString + '\_round' + optTrialString + '\.txt', shell=True)

	print("Replica: " + str(repID))
	print(result135)
	tempcurvefitresults = result135[0]
	#print "tempcurvefitresults = $tempcurvefitresults"
	tempcurvefitresults #=~s/(\]|\[)//g;
	#print "$tempcurvefitresults/n";	
