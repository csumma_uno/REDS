awk '{print 0.0, $4}' REDS.all > hist.in
grep -v 0.000 hist.in > hist.in2
awk '{print $1, $2}' REDS.all > hist.in1
awk '{print 1.0, $3}' REDS.all > hist.in
grep -v 0.000 hist.in > hist.in3
cat  hist.in2 hist.in1 hist.in3 > hist.in4
./histogram_and_average.pl hist.in4 0.1 -0.05 1.05 > fit.in
rm hist.in*
./fit.pl fit.in > fit.out
awk '{print $1}' fit.out > tmp.a
awk '{print $2}' fit.out > tmp.b
awk '{print $3}' fit.out > tmp.c
newa=$(< tmp.a)
newb=$(< tmp.b)
newc=$(< tmp.c)
awk -v var=$newa 'BEGIN {OFS=FS=" "}  {if ($1=="a_reds")sub($3,var); print }' sim.mdp > tmpa.mdp
awk -v var=$newb 'BEGIN {OFS=FS=" "}  {if ($1=="b_reds")sub($3,var); print }' tmpa.mdp > tmpb.mdp
awk -v var=$newc 'BEGIN {OFS=FS=" "}  {if ($1=="c_reds")sub($3,var); print }' tmpb.mdp > tmpc.mdp
mv tmpc.mdp  sim.mdp
rm tmp*
