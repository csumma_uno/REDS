tail -1 REDS*.txt > tmp.1
awk '{print $1}' tmp.1 > tmp.2
newzeta=$(< tmp.2)
awk -v var=$newzeta 'BEGIN {OFS=FS=" "}  {if ($1=="zeta_init_reds")sub($3,var); print }' sim.mdp > tmp.mdp
mv tmp.mdp sim.mdp
rm tmp.*
